#include <FastLED.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include "Adafruit_CCS811.h"

#define DATA_PIN    6
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
#define NUM_LEDS    344
CRGB leds[NUM_LEDS];

#define BRIGHTNESS  192
#define FRAME_DELAY 2

bool isDoorOpen = false;

Adafruit_BME280 bme1;
Adafruit_BME280 bme2;
Adafruit_CCS811 ccs;

#define FAN1 5
#define FAN2 9
#define MAX_CO2 500

int fanspeed = 0;

void setup() {
  Serial.begin(9600);
  Serial.println("Setup");
  delay(3000); // 3 second delay for recovery
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(BRIGHTNESS);
  
  if (!bme1.begin(0x76)) Serial.println("BME280-1 not found");
  if (!bme2.begin(0x77)) Serial.println("BME280-2 not found");
  if (!ccs.begin(0x5B)) Serial.println("CSS not found");

  pinMode(FAN1, OUTPUT);
  pinMode(FAN2, OUTPUT);
}
  
void loop()
{
  handleSensors();
  for (int i = 0; i < NUM_LEDS; i++){
    handleLeds();
  }
}

void handleLeds(){
  if (isDoorOpen){
    scroll(255, 255, 255);
  }else{
    scroll(255, 000, 255);
  }
  FastLED.show();  
  FastLED.delay(FRAME_DELAY); 
}

void scroll(uint8_t r, uint8_t g, uint8_t b){
  leds[0].red =   r;
  leds[0].green = g;
  leds[0].blue =  b;
  for (int i = 1; i < NUM_LEDS; i++){
    leds[i] = leds[i-1];
  }
}

void handleSensors(){
  int co2 = getCO2();
  Serial.print("temp1: " + String(bme1.readTemperature()) + " C,");
  Serial.print(" temp2: " + String(bme2.readTemperature()) + " C,");
  Serial.print(" co2: " + String(co2) + "ppm,");
  Serial.println(" fanspeed: " + fanspeed);
  
  
  if (co2 > MAX_CO2){
    fanspeed = (co2 - MAX_CO2);
    if (fanspeed > 255) { fanspeed = 255; }
  }else{
    fanspeed = 0;
  }
  setFans(fanspeed);
}

void setFans(int amount){
  digitalWrite(FAN1, amount);
  digitalWrite(FAN2, amount);
}

int getCO2(){
  if(ccs.available()){
    if(!ccs.readData()){
      return ccs.geteCO2();
    }
  }
  return -1;
}
